# Kubernetes Up and Running Notes
My notes from the book [Kubernetes Up and Running](http://shop.oreilly.com/product/0636920043874.do) by Brendan Burns, Kelsey Hightower and Joe Beda.

1. [Introduction](Chapter1.md) [incomplete]
2. [Creating and Running Containers](Chapter2.md)